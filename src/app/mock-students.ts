import {Student} from './student';
export const STUDENTS: Student[] = [
    {id: 1, name: 'Samuel', age: 12, class:"7"}, 
    {id: 2, name: 'Mutemi', age: 12, class:"8"}, 
    {id: 3, name: 'Mark', age: 14, class:"5"}, 
    {id: 4, name: 'Mary', age: 11, class:"7"}
  ];