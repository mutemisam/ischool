import { Component, OnInit } from '@angular/core';
import {Student} from '../student';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.css']
})
export class StudentTableComponent implements OnInit {
  students: Student[];
  constructor(private ss: StudentsService) { }

  ngOnInit() {
    this.getAllStudents();
  }
  getAllStudents(): void {
    this.students = this.ss.getAll();
  }

}
